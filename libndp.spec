Name:      libndp
Version:   1.8
Release:   3
Summary:   Library for Neighbor Discovery Protocol
License:   LGPLv2+
URL:       http://www.libndp.org/
Source:    http://www.libndp.org/files/libndp-%{version}.tar.gz

Patch0:        backport-CVE-2024-5564.patch

BuildRequires: gcc
BuildRequires: make

%description
This package contains a library which provides a wrapper
for IPv6 Neighbor Discovery Protocol. It also provides a tool
named ndptool for sending and receiving NDP messages.

%package devel
Summary: Libraries and header files for libndp development
Requires: libndp = %{version}-%{release}

%description devel
This package contains files and libraries for libndp.

%package help
Summary: Document files for libndp

%description help
Document files for libndp.

%prep
%autosetup -p1

%build
%configure
%make_build

%install
%make_install
%delete_la_and_a

%ldconfig_scriptlets

%files
%doc COPYING
%{_libdir}/*so.*
%{_bindir}/ndptool

%files devel
%{_includedir}/*
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc

%files help
%{_mandir}/man8/ndptool.8*

%changelog
* Tue Jun 11 2024 xingwei <xingwei14@h-partners.com> - 1.8-3
- Type:CVE
- ID:CVE-2024-5564
- SUG:NA
- DESC:fix CVE-2024-5564

* Fri Oct 21 2022 gaihuiying <eaglegai@163.com> - 1.8-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:add make as BuildRequires for local rpmbuild

* Sat Mar 19 2022 xihaochen<xihaochen@h-partners.com> - 1.8-1
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:update libndp version to 1.8

* Thu May 27 2021 lijingyuan<lijingyuan3@huawei.com> - 1.7-4
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:Add the compilation dependency of gcc. 

* Mon Sep 16 2019 liyongqiang<liyongqiang10@huawei.com> - 1.7-3
- Package init
